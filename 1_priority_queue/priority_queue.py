class PriorityQueue:
    def __init__(self):
        self.items = []

    def add(self, item, priority):
        adding_item = QueueItem(item, priority)
        self.items.append(adding_item)
        numer_in_queue = self.items.index(adding_item)
        adding_item.set_own_number_in_queue(numer_in_queue)

    def get_first(self):
        self.items.sort(key=self._criteria)
        if len(self.items) == 0:
            raise ValueError("Queue is empty")
        
        get_item = self.items[0]
        self.items.remove(get_item)
        return get_item
    
    def check_first(self):
        self.items.sort(key=self._criteria)
        if len(self.items) == 0:
            raise ValueError("Queue is empty")
        checked_item = self.items[0]
        return checked_item
    
    def is_empty(self):
        if len(self.items) == 0:
            return True
        else:
            return False
        
    def get_length(self):
        return len(self.items)

    def _criteria(self, item):
        return (-item.priority, item.number_in_queue)

class QueueItem:
    def __init__(self, item, priority):
        self.item = item
        self.priority = priority
        self.number_in_queue = None
    
    def set_own_number_in_queue(self, numer_in_queue):
        self.number_in_queue = numer_in_queue

